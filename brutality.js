// the tim hash function ----------------------------------------
const calculateHash = str => {
    let hash = str
        .split("")
        .map((c, i) => str.charCodeAt(i)).map(c => c + 2)
        .map(c => String.fromCharCode(c)).join("");
    return Buffer.from(hash).toString("base64");
};
// --------------------------------------------------------------
// 
// The "fs" module provides a lot of very useful functionality
// to access and interact with the FILE SYSTEM.
const fs = require('fs');

// The "readLine" should be supplied with a stream from "fs"
const readline = require('readline');

// The object that will read from the stream using createInterface()
const rl = readline.createInterface({
    //   Create a reading stream word by word on my txt
    input: fs.createReadStream('crunchlist.txt'),
    // output: process.stdout, // fancy
});
//       rl      =>  Interface
//       on      =>  method
//     'line'    =>  event
//   (password)  =>  listener
rl.on('line', (password) => {
    // For every password of the crunchlist, test the hash function 
    const word = calculateHash(password);
    // Display the output (line tested()
    // console.log(word); // fancy

    // if the exit match the hashed word
    if (word == 'cnFrbg==') {
        // display the password unhashed
        console.log("-----------------------------");
        console.log("    for the hash : ", word, "\n the password is : ",password);
        console.log("-----------------------------");
        return process.exit(1);
    }
})
rl.on('close', () => {
    console.log('done');
})
